import csv
import os


def read_file(name):
    print("Loading File ", name, "Words: ", buf_count(name))
    mySet = set()

    with open(name) as f:
        reader = csv.reader(f)
        for row in reader:
            mySet.add(row[0])
    return mySet


def buf_count(filename):
    f = open(filename)
    lines = 0
    buf_size = 1024 * 1024
    read_f = f.read  # loop optimization

    buf = read_f(buf_size)
    while buf:
        lines += buf.count('\n')
        buf = read_f(buf_size)

    return lines


def read_lexicon(name='lexicon_limpio.csv'):
    print("Loading File: ", name, "Tuplets: ", buf_count(name))

    diccionary = dict()

    with open(name, newline='', encoding='utf-8') as f:
        reader = csv.reader(f, delimiter=';')
        for row in reader:
            diccionary[row[0]] = row[1]
    return diccionary


def read_lexicono(name='lexicono.csv'):
    print("Loading File: ", name, "Tuplets: ", buf_count(name))

    diccionary = dict()

    with open(name, newline='', encoding='utf-8') as f:
        reader = csv.reader(f, delimiter=' ')
        for row in reader:
            diccionary[row[0]] = row[1]
    return diccionary


def read_emos(folder='emo'):
    allEmos = dict()
    for dirname, dir_names, filenames in os.walk(folder):
        for file in filenames:
            simple_name = file.replace(".csv", "")
            allEmos[simple_name] = read_file(os.path.join(dirname, file))
    return allEmos


def switch_polarity(polarity):
    return {
        'positivo': 1,
        'pos': 1,
        'negativo': -1,
        'neg': -1
    }.get(polarity, 0)


def polarity(value):
    if value > 0:
        return 'positiva'
    elif value == 0:
        return 'neutra'
    else:
        return 'negativa'


emotions = read_emos()
badWords = read_file('badWords2-EXT.csv')
lexicon = read_lexicon()
lexiIcons = read_lexicono()

while True:
    userInput = input('Ingrese una oracion para evaluar o presione enter para usar la oracion por defecto:\n')
    if len(userInput) < 2:
        oracion = "es muy admirable ser puro y limpio con la wea :D"
        break
    elif len(userInput) > 2:
        oracion = userInput
        break

print(f'\nPara la oracion:\n\t{oracion}')
sentence = set(oracion.split(" "))

# set emotions in the sentence
sum = dict()
for name in emotions.keys():
    temp = emotions.get(name) & sentence
    if temp:
        sum[name] = temp
        print(f'La emocion {name} a sido encontrada {len(sum[name])} veces')

# get bad words
NAME_BADWORDS = 'badwords'
sum[NAME_BADWORDS] = sentence & badWords

if sum[NAME_BADWORDS]:
    print(f'Se han encontrado {len(sum[NAME_BADWORDS])} mala(s) palabra(s)')

# get polarity with the lexicon
sum_polarity = set()
polarity_count = 0
for word in sentence:
    temp = lexicon.get(word)
    if temp:
        reg = word, temp
        sum_polarity.add(reg)
        polarity_count += switch_polarity(temp)

    temp2 = lexiIcons.get(word)
    if temp2:
        reg = word, temp2
        sum_polarity.add(reg)
        polarity_count += switch_polarity(temp2)

print(f'La polaridad de la oracion es: {polarity(polarity_count)} ({polarity_count})')

sum['polarity'] = sum_polarity
print("Encontrado: ", sum)
